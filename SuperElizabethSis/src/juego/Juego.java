
package juego;

import java.awt.Color;
import java.util.ArrayList;

import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego
{
	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;
	private java.awt.Image fondo;
	private java.awt.Image princess;
	private java.awt.Image fuego;
	private java.awt.Image sold;
	private java.awt.Image gameOver;
	private java.awt.Image win;
	private java.awt.Image obstacul;

	// Variables y métodos propios de cada grupo
	private ArrayList<BolaFuego> disp;
	private ArrayList<Soldado> soldados;
	private Obstaculo [] obstaculos; 
	private Princesa eliz;
	private int vidas;
	private int puntaje;
	private int posInicial;
	private int tiempoTick;
	private int tiempoReal;
	private int indiceSoldado;
	private boolean topeSalto;
	private boolean seEliminoSoldado;



	
	
	Juego()
	{
		// Inicializa el objeto entorno
		this.entorno = new Entorno(this, "Super Elizabeth Sis - Chamizo - Gallo - Saucedo - v1", 800, 600);		
		
		// Inicializar lo que haga falta para el juego
		cargaImagenes();
		
		this.eliz  = new Princesa(150, 485, 45, 20);
		this.disp = new ArrayList<BolaFuego>();
		this.soldados=new ArrayList<Soldado>();
	    this.obstaculos= new Obstaculo[4]; 
		crearObstaculos();
		this.vidas = 3;
		this.puntaje = 0;
		this.topeSalto = false;
		this.seEliminoSoldado = false;
		this.posInicial = this.eliz.getY();
		instanciarSoldados();
		indiceSoldado = 1;

									
		// Inicia el juego!
			
		this.entorno.iniciar();

	}

	/**
	 * Durante el juego, el método tick() será ejecutado en cada instante y 
	 * por lo tanto es el método más importante de esta clase. Aquí se debe 
	 * actualizar el estado interno del juego para simular el paso del tiempo 
	 * (ver el enunciado del TP para mayor detalle).
	 */
	public void tick(){
		// Procesamiento de un instante de tiempo
		
		
		if (!terminado() && !gano()) {
			fondo();
			cantVidas();
			puntaje();
			
			dibujarMoverObstaculos();
			retornarObstaculos();
			
			setearSeMueveSoldado();
			dibujarSoldados();
			reaparecerSoldado();
			
			movimientoPrincesa();
			disparoPrincesa();
			
			colisionPrincesaSoldado();
			colisionDisparoSoldado();
			colisionPrincesaObstaculos();
		
		}
		if(terminado()){
			fondo();
			gameOver();
			cantVidas();
			puntaje();
		}
		if(gano()){
			fondo();
			win();
			cantVidas();
			puntaje();
		}
		
	}
	
	//dibuja el fondo de nuestro juego
	public void fondo() {
		this.entorno.dibujarImagen(fondo, 900, 50, 0, 1.80);
	}
	public void gameOver() {
		this.entorno.dibujarImagen(gameOver, 430, 270, 0, 0.50);
	}
	
	public void win() {
		this.entorno.dibujarImagen(win,420, 270, 0, 0.50);
	}
	
	//contiene los movimientos que puede realizar la princesa
	public void movimientoPrincesa() {
		this.eliz.dibujar(this.entorno,princess);
		
		if(this.entorno.estaPresionada(this.entorno.TECLA_DERECHA) && eliz.getX()<this.entorno.ancho()-30) {
			eliz.moverDerecha();
		}
		if(this.entorno.estaPresionada(this.entorno.TECLA_IZQUIERDA) && eliz.getX()>30) {
			eliz.moverIzquierda();
		}
		if(this.entorno.sePresiono(this.entorno.TECLA_ARRIBA) && !topeSalto) {
			while(this.eliz.getY() > 300) {
				this.eliz.subir();
			}
			this.topeSalto = true;	
		}
		if(topeSalto) {
			if(this.eliz.getY() < posInicial) {
				this.eliz.caer();
			}
		}
		if (this.eliz.getY() == posInicial) {
			this.topeSalto = false;
		}

	}

	//disparo de la princesa
	public void disparoPrincesa() {
		if(this.entorno.sePresiono(this.entorno.TECLA_ESPACIO)) {
			this.disp.add(new BolaFuego(this.eliz.getX(),this.eliz.getY()-15, 10));
		}
		
		for (BolaFuego bolaFuego : disp) {
				bolaFuego.dibujar(this.entorno, fuego);
				bolaFuego.mover();
		}
	}
	
	//Muestra la cantidad de vidas que tiene la princesa
	public void cantVidas() {
		String cantVidas= "Vidas: "+ this.vidas;
		this.entorno.cambiarFont("Calibri", 25, Color.white);
		this.entorno.escribirTexto(cantVidas, 10, 30);

	}
	
	//Muestra el puntaje que tiene la princesa
	public void puntaje() {
		String puntaje= "Puntaje: "+ this.puntaje;
		this.entorno.cambiarFont("Calibri", 25, Color.white);
		this.entorno.escribirTexto(puntaje, 10, 55);
	}
	
	private int random() {
		int valorEntero = (int) Math.floor(Math.random() * (12 - 1 + 1) + 1);//13
		return valorEntero;
	}
	
	private void setearSeMueveSoldado() {
		if (this.soldados.size()!=0 && this.soldados.get(0) != null) {
			this.soldados.get(0).setSeMueve(true); 
		}
		tiempoTick++;
		if (tiempoTick % 60 == 0) {
			tiempoReal++;
			if (tiempoReal % random() == 0 && this.soldados.size() > indiceSoldado) {
				this.soldados.get(indiceSoldado).setSeMueve(true);
				indiceSoldado++;
			}
		}
	}

	private void instanciarSoldados() {
		for (int i = 0; i < 5; i++) {
			Soldado sol = new Soldado(900, 495, 35, 55);
			this.soldados.add(sol);
		}
	}

	private void dibujarSoldados() {
		for (Soldado soldado : soldados) {
			soldado.dibujar(this.entorno,sold);
			if (soldado.getSeMueve() == true) {
				soldado.mover();
			}
		}
		
		for (int i = 0; i < soldados.size(); i++) {
			if (soldados.get(i).getX() == 0) {
				soldados.remove(soldados.get(i));
				seEliminoSoldado = true;
			}
		}
	}

	private void reaparecerSoldado() {
		if (seEliminoSoldado) {
			Soldado nuevo = new Soldado(900, 495, 35, 55);
			soldados.add(nuevo);
		}
	}
	
	public void crearObstaculos() {
		for(int i=0; i<obstaculos.length; i++) { 
			this.obstaculos[i]= new Obstaculo(600+210*i,483,34,70);
		}
	}
	
	public void dibujarMoverObstaculos() {
		for(int i=0;i<obstaculos.length;i++) {
			if(this.obstaculos[i] != null) {
				this.obstaculos[i].dibujarObstaculos(this.entorno, obstacul);
				this.obstaculos[i].moverMisObstaculos();
			}
		}
	}
	
	public void retornarObstaculos() {
		for(int i=0;i<obstaculos.length;i++) {
			if(obstaculos[i].getX()==0) {
				this.obstaculos[i]=new Obstaculo(830,483,34,70); 
				this.obstaculos[i].dibujarObstaculos(entorno, obstacul);
				this.obstaculos[i].moverMisObstaculos();
			}
		}
	} 

	public boolean colisionPrincesaSoldado() {
		for (int i = 0; i < this.soldados.size(); i++) {
			if (this.eliz != null)
				if (this.eliz.getX() - this.eliz.getAncho() / 2 <= this.soldados.get(i).getX() + this.soldados.get(i).getAncho() / 2
						&& this.eliz.getX() + this.eliz.getAncho() / 2 >= this.soldados.get(i).getX() - this.soldados.get(i).getAncho() / 2
						&& this.eliz.getY() + this.eliz.getAlto() / 2 >= this.soldados.get(i).getY() - this.soldados.get(i).getAncho() / 2
						&& this.eliz.getY() - this.eliz.getAlto() <= this.soldados.get(i).getY() + this.soldados.get(i).getAlto()) {
					this.soldados.remove(i);
					vidas--;
					return true;

				}
		}
		return false;
	}
	
	public boolean colisionDisparoSoldado() {
		for (int i = 0; i < this.soldados.size(); i++) {
			for (int j = 0; j < disp.size(); j++) {
				if (disp.get(j).getX() - this.eliz.getAncho() / 2 <= this.soldados.get(i).getX() + this.soldados.get(i).getAncho() / 2
						&& disp.get(j).getX() + this.eliz.getAncho() / 2 >= this.soldados.get(i).getX() - this.soldados.get(i).getAncho() / 2
						&& disp.get(j).getY() + this.eliz.getAlto() / 2 >= this.soldados.get(i).getY() - this.soldados.get(i).getAncho() / 2
						&& disp.get(j).getY() - this.eliz.getAlto() <= this.soldados.get(i).getY() + this.soldados.get(i).getAlto()) {
					this.soldados.remove(i);
					disp.remove(j);
					puntaje = puntaje+5;
					return true;
				}	
			}
		}
		return false;
	}
	
	public boolean colisionPrincesaObstaculos() {
		for(int i=0;i<obstaculos.length;i++) {
			if( this.eliz.getX()==this.obstaculos[i].getX() && this.eliz.getY()>= this.obstaculos[i].getY()) {
				vidas--;
				return true;
			}
		}
		return false;
	}
	
	
	public boolean terminado() {
		if(this.vidas == 0) {
			return true;
		}else {
			return false;
		}
	}
	
	public boolean gano() {
		if(this.puntaje == 50) {
			return true;
		}else {
			return false;
		}
	}
	
	public void cargaImagenes() {
		this.fondo = Herramientas.cargarImagen("fondoJuego.png");
		this.princess = Herramientas.cargarImagen("princessJuego.png");
		this.fuego = Herramientas.cargarImagen("fuegoJuego.png");
		this.sold = Herramientas.cargarImagen("soldado.png");
		this.gameOver = Herramientas.cargarImagen("gameOver.png");
		this.win = Herramientas.cargarImagen("bigWin.png");
		this.obstacul=Herramientas.cargarImagen("obstaculoJuego.png");
	
	}

	
	@SuppressWarnings("unused")
	public static void main(String[] args)
	{
		Juego juego = new Juego();
	}
}

