package juego;

import java.awt.Image;
import entorno.Entorno;

public class Princesa {
	//variables de instancia
		private int x;
		private int y;
		private int alto;
		private int ancho;
		private int velocidad;
		
		//constructor
		Princesa(int x, int y, int alto, int ancho){
			this.x = x;
			this.y  =y;
			this.alto=alto;
			this.ancho=ancho;
			this.velocidad=5;
		}

		public void moverDerecha(){
			this.x= this.x + velocidad;
		}

		public void moverIzquierda(){
			this.x= this.x - velocidad;
		}

		public void dibujar(Entorno e, Image im){
			e.dibujarImagen(im, this.x, this.y-17, 0,0.055 );
		}

		public int getX() {
			return this.x;
		}
		public void setX(int x) {
			this.x = x;
		}

		public int getY() {
			return this.y;
		}
		
		public void setY(int y) {
			this.y = y;
		}

		public int getVelocidad() {
			return velocidad;
		}

		public void setVelocidad(int velocidad) {
			this.velocidad = velocidad;
		}
		
		public int getAlto() {
			return this.alto;
		}

		public void setAlto(int alto) {
			this.alto = alto;
		}
		
		public int getAncho() {
			return this.ancho;
		}

		public void setAncho(int ancho) {
			this.ancho = ancho;
		}
		
		public void subir() {
			this.y -=3;
		}
		
		public void caer() {
			this.y +=3;
			this.x += 1;
		}
		
	
	}

