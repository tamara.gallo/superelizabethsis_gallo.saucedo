package juego;

import java.awt.Image;	

import entorno.Entorno;

public class Soldado {
	//variables de instancia
	private int x;
	private int y;
	private int alto;
	private int ancho;
	private int velocidad;
	private boolean seMueve;
	
	//constructor
	Soldado(int x, int y, int alto, int ancho){
		this.x = x;
		this.y  =y;
		this.alto=alto;
		this.ancho=ancho;
		this.velocidad=1;
		seMueve = false;
	}

	public void mover() {
		this.x = this.x - this.velocidad;
	}

	public void dibujar(Entorno e, Image im){
		e.dibujarImagen(im, this.x, this.y-18, 0, 1.4);
	}
	public int getX() {
		return this.x;
	}
	
	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return this.y;
	}
	
	public void setY(int y) {
		this.y = y;
	}

	public int getVelocidad() {
		return velocidad;
	}

	public void setVelocidad(int velocidad) {
		this.velocidad = velocidad;
	}
	
	public int getAlto() {
		return this.alto;
	}

	public void setAlto(int alto) {
		this.alto = alto;
	}
	
	public int getAncho() {
		return this.ancho;
	}

	public void setAncho(int ancho) {
		this.ancho = ancho;
	}

	public boolean getSeMueve() {
		return seMueve;
	}

	public void setSeMueve(boolean seMueve) {
		this.seMueve = seMueve;
	}

	@Override
	public String toString() {
		return "Soldado [x=" + x + ", y=" + y + ", alto=" + alto + ", ancho=" + ancho + ", velocidad=" + velocidad
				+ "]";
	}
	
	
}
