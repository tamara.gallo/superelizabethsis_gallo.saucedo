package juego;

import java.awt.Image;


import entorno.Entorno;

public class Obstaculo {//comentario
		//variables de instancia
		private double x;
		private int y;
		private int alto;
		private int ancho;
		private double velocidad;
		
		
		//constructor
		public Obstaculo(int x, int y, int ancho, int alto){
			this.x = x;
			this.y = y;
			this.ancho=ancho;
			this.alto=alto;
			this.velocidad=1;
		
			
		}
	
		public void moverMisObstaculos() {
			this.x= this.x - this.velocidad; //la posicion de mi x menor a la de mi velidad
			
		}	
							
		public void dibujarObstaculos(Entorno e,Image imagen) {//como par�metro a entorno y una imagen
			
			
			e.dibujarImagen(imagen, this.x, this.y, 0, 0.23);
		}

		public double getX() {
			return this.x;
		}
	
		public void setX(int x) {
			this.x = x;
		}

		public int getY() {
			return this.y;
		}
		
		public void setY(int y) {
			this.y = y;
		}

		public double getVelocidad() {
			return velocidad;
		}

		public void setVelocidad(int velocidad) {
			this.velocidad = velocidad;
		}
		
		public int getAlto() {
			return this.alto;
		}

		public void setAlto(int alto) {
			this.alto = alto;
		}
		
		public int getAncho() {
			return this.ancho;
		}

		public void setAncho(int ancho) {
			this.ancho = ancho;
		}
		

		

}
		
		
	





