package juego;

import java.awt.Image;
import entorno.Entorno;

public class BolaFuego {
	private int x;
	private int y;
	private int diametro;
	private int velocidad;
	
	BolaFuego(int x, int y, int diametro){
		this.x=x;
		this.y=y;
		this.diametro=diametro;
		this.velocidad=+2;
	}
	
	public void mover() {
		this.x = this.x  + velocidad;
	}
	
	public void dibujar(Entorno e,Image i) {
		e.dibujarImagen(i, this.x, this.y, 9.5, 0.06);
		
	}

	public int getX() {
		return this.x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return this.y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getDiametro() {
		return this.diametro;
	}

	public void setDiametro(int diametro) {
		this.diametro = diametro;
	}

	public int getVelocidad() {
		return this.velocidad;
	}

	public void setVelocidad(int velocidad) {
		this.velocidad = velocidad;
	}
}
